# Ecrypted Messaging Application  
A chat application developed as part of course assignment of Computer Networks.  
It has three types of configurations:  
- Basic - Normal exchange of messages between multiple users
- Encrypted Chat - Chat messages are encrypted(RSA) so that server doesn't have access to the messages  
- Encrypted + Signature - It also adds a MD5 hash signature with messages to verify the integrity of messages  
  
## Prerequisites  
Python - Main Code  
Java - Cryptography classes - Tested with OpenJDK 11  
Pyjnius - Export Java classes in Python  
Crypto.jar  
  
## Installing  
Installation of Pyjnius : https://pyjnius.readthedocs.io/en/stable/installation.html  
  
Creating Crypto.jar:
```  
javac Crypto.java  
jar -cf Crypto.jar Crypto.class   
```
  
Appropriate values of JAVA_HOME and LD_LIBRARY_PATH must be set for working of pyjnius  
  
## Running  
To run unencrypted version:  
- cd src/unencrypted  
- Start Server : python3 server.py "port" "host_ip"  
- Start a client : python3 client.py "username" "server_ip" "server_port"  
  
To run unencrypted version:  
- cd src/encrypted  
- Start Server : python3 server.py "port" "host_ip"  
- Start a client : python3 client.py "username" "server_ip" "server_port"  
  
To run unencrypted-signature version:  
- cd src/encrypted_signature  
- Start Server : python3 server.py "port" "host_ip"  
- Start a client : python3 client.py "username" "server_ip" "server_port"  
  
## Interface
After successfully starting the server, any number of clients can connect to it. A client can disconnect safely by sending @UNREGISTER ME. The client CLI prompts user for an input after which users can chat with each other.  

Simple working of application:  

Bar's CLI
> Enter a Message(@[recipient username] [message]): @foo Hey there !!  
> SENT foo  
>  
> Enter a Message(@[recipient username] [message]): @baz Hi!!  
> ERROR 102 Unable to send  
>  
> @foo Hi  
>  
> Enter a Message(@[recipient username] [message]): @foo I guess baz is not online, I am leaving as well  
> SENT foo  
>  
> Enter a Message(@[recipient username] [message]): @UNREGISTER ME  
> UNREGISTERED  
>   
  
    
Foo's CLI  
> Enter a Message(@[recipient username] [message]):  
> @bar Hey there  
>  
> Enter a Message(@[recipient username] [message]): @bar Hi  
> SENT Bar  
>  
> @bar I guess baz is not online, I am leaving as well  
>  

## License  
This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
