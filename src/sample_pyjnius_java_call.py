import os
from jnius import autoclass

os.environ['CLASSPATH']="Crypto.jar"
crypt=autoclass("Crypto")
generateKeyPair = crypt.generateKeyPair()

publicKey = generateKeyPair.getPublic().getEncoded()
privateKey = generateKeyPair.getPrivate().getEncoded()
publicKey = publicKey.tostring()
privateKey = privateKey.tostring()

print(publicKey, type(publicKey))

encryptedData = crypt.encrypt(publicKey,"hi there".encode())
decryptedData = crypt.decrypt(privateKey, encryptedData)

print((encryptedData.tostring()).decode())

