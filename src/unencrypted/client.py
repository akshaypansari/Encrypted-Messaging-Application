from socket import *
import sys 
import threading
import re
from base64 import *

class Client:
	def __init__(self, username, server_ip, server_port):
		self.username = username
		self.server_ip = server_ip
		self.server_port = server_port
	
	# Register itself to server
	def register(self):
		self._send_register()
		self._recv_register()

	# Begin sending protocol
	def start_sending(self):
		while(True):
			user_input = input("Enter a Message(@[recipient username] [message]):\n")

			valid, receiver_username, message, error = self._check_valid_message(user_input)
			if not valid:
				print(error)
				continue
		
			send_message = self._create_server_message(receiver_username, message)
			send_message = b64encode(send_message.encode())

			self.send_socket.send(send_message)
			
			server_response = self.send_socket.recv(1024)
			server_response = b64decode(server_response).decode()
			
			print(server_response)
			if server_response=="UNREGISTERED\n\n":
				return 1

	# Begin receiving protocol
	def start_receiving(self):
		while(True):
			# Get Message from server		
			server_message = self.receive_socket.recv(1024)
			server_message = b64decode(server_message).decode()

			# Stop thread if Unregister
			if server_message=="UNREGISTERED\n\n":
				return 1

			is_valid, sender_user, content_len, message = self._break_server_message(server_message)
			if not is_valid:
				response = "ERROR 103 Header incomplete\n\n".encode()
				response = b64encode(response)
				self.receive_socket.send(response)
			else:
				print("\n@" + sender_user + " " + message)

				response = ("RECEIVED " + sender_user + "\n\n").encode()
				response = b64encode(response)
				self.receive_socket.send(response)

				print("\nEnter a Message(@[recipient username] [message]):\n")

	# Break FORWARD message received from server
	def _break_server_message(self, server_message):
		header_split = server_message.split('\n')
		username = header_split[0][8:]
		content_length = header_split[1][16:]
		message = header_split[3]

		if (header_split[0][:8]!="FORWARD " or header_split[1][:16]!="Content-length: " or 
			header_split[2]!="" or not username.isalnum() or 
			str(len(message.encode()))!= content_length):
			return False, "","",""

		return True, username, int(content_length), message

	# Create a SEND message
	def _create_server_message(self, user, message):
		send_message = ""
		send_message += "SEND " + user + "\n"
		send_message += "Content-length: " + str(len(message.encode())) + "\n"#message initially string
		send_message += "\n"
		send_message += message

		return send_message

	# Check if input message is correct
	def _check_valid_message(self, user_input):
		split_input = user_input.split(" ", 1)
		if len(user_input) < 1:
			return False, "", "", "Invalid Format of Message"
		if user_input[0] != "@":
			return False, "", "", "Invalid Format of Message"
		if len(split_input) != 2:
			return False, "", "", "No Message"
		
		receiver_username = split_input[0][1:]
		message = split_input[1].strip()

		if len(receiver_username) == 0 or len(message) == 0:
			return False, "", "", "Invalid Format of Message"
		
		return True, receiver_username, message, "NA"

	# Register TORECV
	def _recv_register(self):
		server_response = ""
		req_server_response = b64encode(("REGISTERED TORECV " + self.username + "\n\n").encode())

		while(server_response != req_server_response):
			self.receive_socket = socket(AF_INET, SOCK_STREAM)
			self.receive_socket.connect((self.server_ip,self.server_port))
			
			receive_command = ("REGISTER TORECV " + self.username + "\n\n").encode()
			receive_command = b64encode(receive_command)

			self.receive_socket.send(receive_command)
			server_response = self.receive_socket.recv(1024)
			if server_response != req_server_response:
				self.receive_socket.close()

	# Register TOSEND
	def _send_register(self):
			server_response = ""
			req_server_response = b64encode(("REGISTERED TOSEND " + self.username + "\n\n").encode())

			while(server_response != req_server_response):
				self.send_socket = socket(AF_INET, SOCK_STREAM)
				self.send_socket.connect((self.server_ip,self.server_port))
			
				send_command = ("REGISTER TOSEND " + self.username + "\n\n").encode()
				send_command = b64encode(send_command)

				self.send_socket.send(send_command)
				server_response = self.send_socket.recv(1024)
				if server_response != req_server_response:
					self.send_socket.close()

		
if __name__ == "__main__" :
	client_username = sys.argv[1]
	server_ip = sys.argv[2]
	server_port = 5050
	if len(sys.argv) == 4:
		server_port = int(sys.argv[3])

	while ((not client_username.isalnum()) or client_username=="UNREGISTER"):
		client_username = input("Please enter proper username: ")

	new_client = Client(client_username, server_ip, server_port); 
	new_client.register() 

	t1 = threading.Thread(target=new_client.start_receiving) 
	t2 = threading.Thread(target=new_client.start_sending)

	t1.start() 
	t2.start() 
