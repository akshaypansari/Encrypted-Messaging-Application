from socket import *
import sys 
import threading
import re
from base64 import * 

DO_ENCRYPTION = 1

class Server:
	def __init__(self, server_port, server_ip):
		self.server_port = server_port
		self.server_socket = socket(AF_INET,SOCK_STREAM)
		self.server_ip = server_ip
		self.server_socket.bind((server_ip,self.server_port))
		
		self.user_to_socket = {}
		self.public_key_table = {}

	# Starts a thread for every incoming connection
	def start_listening(self):
		self.server_socket.listen(1)
		print ('The server is ready to receive\n')

		while True:
			conn_socket, addr = self.server_socket.accept()
			t1 = threading.Thread(target=self._interact,
								  kwargs={'conn_socket':conn_socket})
			t1.start()

	# Interacts with incoming connections
	def _interact(self, conn_socket):

		message = conn_socket.recv(1024)
		message = b64decode(message)
		code, username, server_response = self._parse_register_message(message)

		server_response = b64encode(server_response.encode())
		conn_socket.send(server_response)

		# Register TORECV
		if (code == 1):
			self.user_to_socket[username] = conn_socket

		# Register TOSEND
		elif code == 0:
			while True:
				
				# Ask sender for username and return Public Key
				client_username = b64decode(conn_socket.recv(1024)).decode()
				if client_username in self.public_key_table:
					conn_socket.send( b64encode(self.public_key_table[client_username]) )
				else:
					try:
						conn_socket.send( b64encode("NO KEY FOUND".encode()))
					except Exception as e:
						continue
					continue

				send_message = conn_socket.recv(1024)
				send_message = b64decode(send_message)
				
				# Recieve Signature
				hash_val = conn_socket.recv(1024)

				is_valid, client_username, to_client_message = self._break_message(send_message, username)

				to_client_message = b64encode(to_client_message)
				
				# User Unregister Command 
				if client_username=="UNREGISTER":
					self.user_to_socket[username].send(b64encode(("UNREGISTERED\n\n").encode()))
					conn_socket.send(b64encode(("UNREGISTERED\n\n").encode()))
					conn_socket.close()
					self.user_to_socket[username].close()
					(self.user_to_socket).pop(username, None)
					(self.public_key_table).pop(username, None)
					return 1

				if is_valid:
					if client_username in self.user_to_socket:
						# Forward User message, hash value and public key of sender
						self.user_to_socket[client_username].send(to_client_message)
						self.user_to_socket[client_username].recv(1024)
						
						self.user_to_socket[client_username].send(hash_val)
						self.user_to_socket[client_username].recv(1024)

						pub_key = b64encode(self.public_key_table[username])
						self.user_to_socket[client_username].send(pub_key)
						self.user_to_socket[client_username].recv(1024)
						
						# Response from the receiver
						client_response = self.user_to_socket[client_username].recv(1024)
						client_response = b64decode(client_response).decode()

						if client_response == "RECEIVED " + username + "\n\n":
							conn_socket.send(b64encode(("SENT " + client_username + "\n\n").encode()))
						else:
							conn_socket.send(b64encode(("ERROR 102 Unable to send\n\n").encode()))
					else:
						conn_socket.send(b64encode(("ERROR 102 Unable to send\n\n").encode()))
				else:
					conn_socket.send(b64encode("ERROR 103 Header incomplete\n\n".encode()))

		return 1

	# Breaks the senders message and returns forward message
	def _break_message(self, message, sender_username):
		splitted_messsage = message.split('\n'.encode())
		if(len(splitted_messsage)<4):
			return False, "", ""

		first_line_of_message = splitted_messsage[0].decode()
		second_line_of_message = splitted_messsage[1].decode()
		third_line_of_message = splitted_messsage[2].decode()
		fourth_line_of_message = ("\n".encode()).join(splitted_messsage[3:])

		splitted_first_line = first_line_of_message.split(' ')
		splitted_second_line = second_line_of_message.split(' ')

		if(len(splitted_first_line)!=2):
			return False, "", ""
		if(len(splitted_second_line)!=2):
			return False, "", ""
			
		username = splitted_first_line[1]
		msg_length = splitted_second_line[1]
		if (splitted_first_line[0] != "SEND") or (username.isalnum() == False) or splitted_second_line[0] != "Content-length:"\
			or third_line_of_message!='' or str(len(fourth_line_of_message))!=msg_length:
			return False, "", ""
		
		to_client_message = ("FORWARD " + sender_username + '\n'+ \
							('\n').join([second_line_of_message,
										"\n"])).encode() + fourth_line_of_message
	
		return True, username, to_client_message

	# Parses Register TO messages
	def _parse_register_message(self, message):
		if (DO_ENCRYPTION==1):
			toks = message.split(' '.encode())
			if(len(toks)>3): 
				toks[2] = (' '.encode()).join(toks[2:])

			if toks[0]==b"SENDKEY":
				self.public_key_table[toks[1].decode()] = toks[2]
				return 2, "", "RECEIVED PUBLIC KEY"

		toks = (message.decode()).split()
		if toks[0]=="REGISTER" and toks[1]=="TOSEND":
			if toks[2].isalnum():
				return 0, toks[2], "REGISTERED TOSEND "+toks[2]+"\n\n"
			else:
				return -1, -1, "ERROR 100 Malformed username\n\n"

		elif toks[0]=="REGISTER" and toks[1]=="TORECV":
			if toks[2].isalnum():
				return 1, toks[2], "REGISTERED TORECV "+toks[2]+"\n\n"
			else:
				return -1, -1, "ERROR 100 Malformed username\n\n"
			
		else:
			return -1,-1, "ERROR 101 No user registered\n\n"

if __name__ == "__main__" :
	server_port = 5050
	server_ip = "localhost"
	if len(sys.argv) == 2:
		server_port = int(sys.argv[1])
	if len(sys.argv) == 3:
		server_port = int(sys.argv[1])
		server_ip = sys.argv[2]

	new_server = Server(server_port, server_ip)
	new_server.start_listening()
