from socket import *
import sys 
import threading
import re
from base64 import *
from jnius import autoclass
import os
import hashlib

# Global Cryptography library
os.environ['CLASSPATH']="Crypto.jar"
crypt=autoclass("Crypto")
def generate_public_private_key():
	generateKeyPair = crypt.generateKeyPair()
	public_key = generateKeyPair.getPublic().getEncoded()
	private_key = generateKeyPair.getPrivate().getEncoded()
	return public_key.tostring(), private_key.tostring()


class Client:
	def __init__(self, username, server_ip, server_port, public_key, private_key):
		self.username = username
		self.server_ip = server_ip
		self.server_port = server_port
		self.public_key = public_key
		self.private_key = private_key
	
	# Register itself to server
	def register(self):
		self._send_register()
		self._recv_register()
		self._send_publickey()

	# Begin sending protocol
	def start_sending(self):
		while(True):
			user_input = input("Enter a Message(@[recipient username] [message]):\n")

			valid, receiver_username, message, error = self._check_valid_message(user_input)
			if not valid:
				print(error)
				continue
			
			# Ask server for Public key of receiver to perform encryption
			self.send_socket.send(b64encode(receiver_username.encode()))
			server_response_public_key = self.send_socket.recv(1024)
			server_response_public_key= b64decode(server_response_public_key)

			if (server_response_public_key == b"NO KEY FOUND"):
				print("Cant send Message: no public key found for the user")

			else:
				send_message, encrpyted_hash = self._create_server_message(receiver_username, message, server_response_public_key)

				send_message = b64encode(send_message)
				self.send_socket.send(send_message)
				self.send_socket.send(b64encode(encrpyted_hash))

				server_response = self.send_socket.recv(1024)
				server_response = b64decode(server_response).decode()
				
				print(server_response)

				# Stop thread if Unregister
				if server_response=="UNREGISTERED\n\n":
					return 1

	# Begin receiving protocol
	def start_receiving(self):
		try:
			while(True):
				# Get Message from server
				server_message = self.receive_socket.recv(1024)
				server_message = b64decode(server_message)
				self.receive_socket.send(b"ACK")
				
				# Get Signature from server
				hash_val = self.receive_socket.recv(1024)
				hash_val = b64decode(hash_val)
				self.receive_socket.send(b"ACK")

				# Stop thread if Unregister
				if server_message==b"UNREGISTERED\n\n":
					return 1

				is_valid, sender_user, content_len, message = self._break_server_message(server_message, hash_val)
				if not is_valid:
					response = "ERROR 103 Header incomplete\n\n".encode()
					response = b64encode(response)
					self.receive_socket.send(response)
				else:
					print("\n@" + sender_user + " " + message)

					response = ("RECEIVED " + sender_user + "\n\n").encode()
					response = b64encode(response)
					self.receive_socket.send(response)

					print("\nEnter a Message(@[recipient username] [message]):\n")
		
		except Exception as e:
			return 1

	# Break FORWARD message received from server
	def _break_server_message(self, server_message, hash_val):
		header_split = server_message.split('\n'.encode())
		header_split[0] = header_split[0].decode()
		header_split[1] = header_split[1].decode()
		header_split[2] = header_split[2].decode()
		if(len(header_split)>4):
			header_split[3] = ('\n'.encode()).join(header_split[3:])
		username = header_split[0][8:]
		content_length = header_split[1][16:]
		message = header_split[3]

		# Receive Public Key from server
		server_response_public_key = self.receive_socket.recv(1024)
		self.receive_socket.send(b"ACK")

		# Decrypt Hash 
		server_response_public_key= b64decode(server_response_public_key)
		md5hash = crypt.decryptUsingPublic(server_response_public_key,hash_val).tostring()
		
		# Create Hash
		recv_hash = hashlib.md5(message).digest()

		if (header_split[0][:8]!="FORWARD " or header_split[1][:16]!="Content-length: " or \
			header_split[2]!="" or not username.isalnum() or \
			str(len(message))!= content_length or recv_hash != md5hash): #Compare hashes
			return False, "","",""

		# Decrypt Message
		message = (crypt.decrypt(self.private_key,message))
		message = (message.tostring()).decode()

		return True, username, int(content_length), message
 
	# Create a SEND message
	def _create_server_message(self, user, message, public_key_of_user):
		send_message = ""
		send_message += "SEND " + user + "\n"

		# Encrypt message
		message = crypt.encrypt(public_key_of_user,message.encode())
		message = message.tostring()

		send_message += "Content-length: " + str(len(message)) + "\n"

		send_message += "\n"
		send_message = send_message.encode()
		send_message += message

		#Signature
		hash_val = hashlib.md5(message).digest()
		encrpyted_hash = (crypt.encryptUsingPrivate(self.private_key, hash_val)).tostring()

		return send_message, encrpyted_hash

	# Check if input message is correct
	def _check_valid_message(self, user_input):
		split_input = user_input.split(" ", 1)
		if len(user_input) < 1:
			return False, "", "", "Invalid Format of Message"
		if user_input[0] != "@":
			return False, "", "", "Invalid Format of Message"
		if len(split_input) != 2:
			return False, "", "", "No Message"
		
		receiver_username = split_input[0][1:]
		message = split_input[1].strip()

		if len(receiver_username) == 0 or len(message) == 0:
			return False, "", "", "Invalid Format of Message"
		
		return True, receiver_username, message, "NA"

	# Register TORECV
	def _recv_register(self):
		server_response = ""
		req_server_response = b64encode(("REGISTERED TORECV " + self.username + "\n\n").encode())

		while(server_response != req_server_response):
			self.receive_socket = socket(AF_INET, SOCK_STREAM)
			self.receive_socket.connect((self.server_ip,self.server_port))
			
			receive_command = ("REGISTER TORECV " + self.username + "\n\n").encode()
			receive_command = b64encode(receive_command)

			self.receive_socket.send(receive_command)
			server_response = self.receive_socket.recv(1024)
			if server_response != req_server_response:
				self.receive_socket.close()

	# Send Public Key to server
	def _send_publickey(self):
		server_response = ""
		req_server_response = b64encode(("RECEIVED PUBLIC KEY").encode())

		while(server_response != req_server_response):
			self.public_key_socket = socket(AF_INET, SOCK_STREAM)
			self.public_key_socket.connect((self.server_ip,self.server_port))
			
			sendkey_command = ("SENDKEY " + self.username + " ").encode()
			sendkey_command = sendkey_command + self.public_key
			
			sendkey_command = b64encode(sendkey_command)

			self.public_key_socket.send(sendkey_command)
			server_response = self.public_key_socket.recv(1024)
			if server_response != req_server_response:
				self.public_key_socket.close()

	# Register TOSEND
	def _send_register(self):
			server_response = ""
			req_server_response = b64encode(("REGISTERED TOSEND " + self.username + "\n\n").encode())

			while(server_response != req_server_response):
				self.send_socket = socket(AF_INET, SOCK_STREAM)
				self.send_socket.connect((self.server_ip,self.server_port))
								
				send_command = ("REGISTER TOSEND " + self.username + "\n\n").encode()
				send_command = b64encode(send_command)

				self.send_socket.send(send_command)
				server_response = self.send_socket.recv(1024)
				if server_response != req_server_response:
					self.send_socket.close()


if __name__ == "__main__" :
	client_username = sys.argv[1]
	server_ip = sys.argv[2]
	server_port = 5050
	if len(sys.argv) == 4:
		server_port = int(sys.argv[3])

	while ((not client_username.isalnum()) or client_username=="UNREGISTER"):
		client_username = input("Please enter proper username: ")

	public_key, private_key = generate_public_private_key()
	new_client = Client(client_username, server_ip, server_port, public_key, private_key); 
	new_client.register() 

	# Register special User for unregistering
	public_key_unregister, private_key_unregister = generate_public_private_key()
	unregister_client = Client("UNREGISTER", server_ip, server_port, public_key, private_key); 
	unregister_client.register() 

	t1 = threading.Thread(target=new_client.start_receiving) 
	t2 = threading.Thread(target=new_client.start_sending)

	t1.start() 
	t2.start()
